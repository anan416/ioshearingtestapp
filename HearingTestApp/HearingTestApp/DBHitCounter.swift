//
//  DBHitCounter.swift
//  HearingTestApp
//
//  Created by Andreas Thorslund Andersen on 14/11/2018.
//  Copyright © 2018 Andreas Thorslund Andersen. All rights reserved.
//

import Foundation

class DBHitCounter {
    var db: Int
    var hits = 0
    
    init(dBLevel: Int) {
        db = dBLevel
    }
    
    func setDB(value: Int) {
        db = value
    }
    func getDB() -> Int {
        return db
    }
    
    func getHits() -> Int{
        return hits
    }
    
    func addHit() {
        hits = hits + 1
    }
}
