//
//  TestViewController.swift
//  HearingTestApp
//
//  Created by Persha Pakdast on 25/10/2018.
//  Copyright © 2018 Andreas Thorslund Andersen. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {
    
    @IBOutlet weak var freqLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    
    func updateLabels() {
        freqLabel.text = "Frequency: " + String(screening.thisFreq) + " Hz"
        volumeLabel.text = "Volume: " + String(screening.thisDB) + "dB"
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        startAudio()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(false)
        screening.TestActive = false
        stopAudio()
    }
    
    @objc func fireTimer() {
        print("timer fired")
    }
    
    var screening = OneUpTwoDown()
    
    @IBAction func ScreeningButton(_ sender: UIButton) {
        updateLabels()
        if (!screening.TestActive) {
            DispatchQueue.global(qos: .background).async { self.screening.startTest() }
        }
        if(screening.isActive()) {
            sender.setTitle("I heard that", for: .normal)
            screening.userRegistered()
            
        } else {
            sender.setTitle("Start test", for: .normal)
            
        }
    }
    
    func startOsc(){
        screening.startOsc()
    }
    func stopOsc() {
        screening.stopOsc()
    }
    
    func startAudio() {
        screening.startAudio()
    }
    
    func stopAudio() {
       screening.stopAudio()
    }
    
    
    
}
