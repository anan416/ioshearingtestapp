//
//  FreqObservation.swift
//  HearingTestApp
//
//  Created by Andreas Thorslund Andersen on 14/11/2018.
//  Copyright © 2018 Andreas Thorslund Andersen. All rights reserved.
//

import Foundation

struct FreqObservation {
    var ear = "left"
    var freq = 250
    var dBTreshold = 0
}
