//
//  PureToneScreening.swift
//  HearingTestApp
//
//  Created by Andreas Thorslund Andersen on 14/11/2018.
//  Copyright © 2018 Andreas Thorslund Andersen. All rights reserved.
//

import Foundation
import AudioKit


class PureToneScreening {
    let oscillator = AKOscillator()
    var MAXFREQ: Int {return 250}
    var MAXAMP: Double {return 0.5}
    var TestActive = false
    
    init() {
        oscillator.frequency = 250
        oscillator.amplitude = 2
        AudioKit.output = oscillator
    }
    
    func startAudio() -> String{
        do {
            try AudioKit.start()
            return "started AudioKit"
        } catch {
            return "failed to start"
        }
    }
    
    func stopAudio() -> String{
        do {
            try AudioKit.stop()
            return "Stopped AudioKit"
        } catch {
            return "failed to stop"
        }
    }
    func startOsc() {
        oscillator.start()
    }
    func stopOsc() {
        oscillator.stop()
    }
    
    func oscillatorEnabled() -> Bool {
        return oscillator.isStarted
    }
    func isActive() -> Bool{
        return TestActive
    }
    
    func startTest() {
        //to be implemented in subclasses.
    }
}
